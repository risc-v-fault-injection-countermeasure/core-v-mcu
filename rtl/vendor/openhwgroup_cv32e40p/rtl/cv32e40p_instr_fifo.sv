// Based on the implementation found at:
// https://vlsiverify.com/verilog/verilog-codes/synchronous-fifo/
module cv32e40p_instr_fifo
  import cv32e40p_pkg::*;
(
  input  logic clk,
  input  logic rst_n,

  input  instr_group_e op_group_i,
  input  alu_opcode_e operator_i,
  input  logic  [1:0] mul_signed_i,
  input  logic [31:0] operand_a_i,
  input  logic [31:0] operand_b_i,
  input  logic [31:0] result_i,

  output instr_group_e op_group_o,
  output alu_opcode_e operator_o,
  output logic  [1:0] mul_signed_o,
  output logic [31:0] operand_a_o,
  output logic [31:0] operand_b_o,
  output logic [31:0] result_o,

  output logic ready_o, // The FIFO is ready to receive a new instruction
  input  logic valid_i, // The EX stage has a new instruction to write

  input  logic ready_i, // The EX stage is ready to receive a checkable instruction
  output logic valid_o  // The FIFO has a checkable instruction for the EX stage to read
);

localparam int POINTER_WIDTH = $clog2(INSTR_FIFO_DEPTH);
localparam int INSTR_FIFO_WIDTH = INSTR_GROUP_WIDTH + ALU_OP_WIDTH + 2 + 32 + 32 + 32;

// Pointers have 1 extra bit to differentiate between full and empty
logic [POINTER_WIDTH:0] w_ptr, r_ptr;

logic full;
logic empty;
logic pointers_match;
logic indicator_bit_different;

// The actual stored instructions
logic [INSTR_FIFO_DEPTH-1:0][INSTR_FIFO_WIDTH-1:0] data;

// Detect empty or full state
assign pointers_match = w_ptr[POINTER_WIDTH-1:0] == r_ptr[POINTER_WIDTH-1:0];
assign indicator_bit_different = w_ptr[POINTER_WIDTH] ^ r_ptr[POINTER_WIDTH];
assign full = pointers_match & indicator_bit_different;
assign empty = pointers_match & !indicator_bit_different;

// The ready/valid state depends only on whether the FIFO is empty or full
assign ready_o = !full;
assign valid_o = !empty;

always_ff @(posedge clk or negedge rst_n)
  if (!rst_n)
    w_ptr <= '0;
  else if (valid_i & ready_o)
    w_ptr <= w_ptr + 1;

always_ff @(posedge clk or negedge rst_n)
  if (!rst_n)
    r_ptr <= '0;
  else if (ready_i & valid_o)
    r_ptr <= r_ptr + 1;

// Data, mainly the operator, operands, and result.
always_ff @(posedge clk or negedge rst_n) begin
  if (!rst_n)
    data  <= '0;

  else begin
    if (valid_i & ready_o) begin
      // Write the current instruction to be checked later
      data[w_ptr[POINTER_WIDTH-1:0]][1:0]    <= op_group_i;
      data[w_ptr[POINTER_WIDTH-1:0]][8:2]    <= operator_i;
      data[w_ptr[POINTER_WIDTH-1:0]][10:9]   <= mul_signed_i;
      data[w_ptr[POINTER_WIDTH-1:0]][42:11]  <= operand_a_i;
      data[w_ptr[POINTER_WIDTH-1:0]][74:43]  <= operand_b_i;
      data[w_ptr[POINTER_WIDTH-1:0]][106:75] <= result_i;
    end

    if (valid_o) begin
      // Read a stored operation to be checked in this cycle
      op_group_o   <= data[r_ptr[POINTER_WIDTH-1:0]][1:0];
      operator_o   <= data[r_ptr[POINTER_WIDTH-1:0]][8:2];
      mul_signed_o <= data[r_ptr[POINTER_WIDTH-1:0]][10:9];
      operand_a_o  <= data[r_ptr[POINTER_WIDTH-1:0]][42:11];
      operand_b_o  <= data[r_ptr[POINTER_WIDTH-1:0]][74:43];
      result_o     <= data[r_ptr[POINTER_WIDTH-1:0]][106:75];
    end
  end
end

endmodule
